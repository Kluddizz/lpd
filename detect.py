from sys import platform
import shutil
import pytesseract
import numpy as np

from models import *
from utils.datasets import *
from utils.utils import *

def detect_bboxes(image, conf_thres=0.5, nms_thres=0.45):
    results = []

    weights = "./weights_lpr.pt"
    cfg = "./cfg/yolov3.cfg"
    img_size = 416

    device = torch_utils.select_device()

    # Initialize model
    model = Darknet(cfg, img_size)

    # Load weights
    if weights.endswith('.pt'):  # pytorch format
        if weights.endswith('yolov3.pt') and not os.path.exists(weights):
            if (platform == 'darwin') or (platform == 'linux'):
                os.system('wget https://storage.googleapis.com/ultralytics/yolov3.pt -O ' + weights)
        model.load_state_dict(torch.load(weights, map_location='cpu')['model'])
    else:  # darknet format
        load_darknet_weights(model, weights)

    model.to(device).eval()

    # Get classes and colors
    classes = load_classes(parse_data_cfg('cfg/coco.data')['names'])
    colors = [[random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)] for _ in range(len(classes))]

    img, _, _, _ = letterbox(image, height=img_size)
    img = img[:, :, ::-1].transpose(2, 0, 1)
    img = np.ascontiguousarray(img, dtype=np.float32)
    img /= 255.0
    
    # Get detections
    img = torch.from_numpy(img).unsqueeze(0).to(device)
    pred = model(img)
    pred = pred[pred[:, :, 4] > conf_thres]  # remove boxes < threshold

    if len(pred) > 0:
        # Run NMS on predictions
        try:
            detections = non_max_suppression(pred.unsqueeze(0), conf_thres, nms_thres)[0]

            # Rescale boxes from 416 to true image size
            detections[:, :4] = scale_coords(img_size, detections[:, :4], image.shape)

            # Print results to screen
            unique_classes = detections[:, -1].cpu().unique()
            for c in unique_classes:
                n = (detections[:, -1].cpu() == c).sum()

            # Draw bounding boxes and labels of detections
            for x1, y1, x2, y2, conf, cls_conf, cls in detections:
                results.append((int(x1), int(y1), int(x2), int(y2)))
        except AssertionError as error:
            print(error)
    
    return results