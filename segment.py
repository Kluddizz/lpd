import cv2
import detect
from skimage import measure
from skimage.measure import regionprops
import matplotlib.patches as patches
import numpy as np
import PIL
import pytesseract

import matplotlib.pyplot as plt

def find_characters(license_plate):
    gray = cv2.cvtColor(license_plate, cv2.COLOR_RGB2GRAY)
    #ret, thresh = cv2.threshold(gray, 50, 255, cv2.THRESH_BINARY)
    #thresh_mean = cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 5, 10)
    thresh_gauss = cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 51, 27)
    
    ratio = 200.0 / thresh_gauss.shape[1]
    dim = (200, int(thresh_gauss.shape[0] * ratio))
    resizedCubic = cv2.resize(thresh_gauss, dim, interpolation=cv2.INTER_CUBIC)

    border_size = 10
    border = cv2.copyMakeBorder(resizedCubic, top=border_size, bottom=border_size, left=border_size, right=border_size, borderType=cv2.BORDER_CONSTANT, value=[255, 255, 255])

    edges = cv2.Canny(border, 50, 150, apertureSize=3)
    lines = cv2.HoughLinesP(image=edges, rho=1, theta=np.pi / 180, threshold=80, lines=np.array([]), minLineLength=100, maxLineGap=80)

    a, b, c = lines.shape

    for i in range(a):
        x = lines[i][0][0] - lines[i][0][2]
        y = lines[i][0][1] - lines[i][0][3]

        if x != 0 and abs(y / x) < 1:
            cv2.line(border, (lines[i][0][0], lines[i][0][1]), (lines[i][0][2], lines[i][0][3]), (255, 255, 255), 1, cv2.LINE_AA)
    
    se = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (2, 2))
    gray = cv2.morphologyEx(border, cv2.MORPH_CLOSE, se)

    config = '-l eng --oem 1 --psm 3'
    text = pytesseract.image_to_string(gray, config=config)
    text_cleaned = ""

    alphabeth = "ABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890"

    for c in text:
        if c in alphabeth:
            text_cleaned += c
        else:
            text_cleaned += ' '

    return ' '.join(text_cleaned.split())

image = cv2.imread("./img/18.jpg")
image_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
bboxes = detect.detect_bboxes(image)

fig, (ax1) = plt.subplots(1)
ax1.imshow(image_rgb)

for x1, y1, x2, y2 in bboxes:
    cropped = image[y1:y2, x1:x2]
    cropped_rgb = cv2.cvtColor(cropped, cv2.COLOR_BGR2RGB)
    cropped_gray = cv2.cvtColor(cropped, cv2.COLOR_BGR2GRAY)

    text = find_characters(cropped)

    border = patches.Rectangle((x1, y1), x2-x1, y2-y1, linewidth=2, edgecolor='red', fill=False)
    ax1.add_patch(border)
    ax1.annotate(text, (x1, y1), color='white', fontsize=8, va='bottom', bbox={'pad': 1, 'color': 'red'})
    
plt.show()